package com.ollo.exam.cheque;

/**
 * Implementation of Cheque interface.
 * <p/>
 * Make your changes here.
 */
public class MyCheque implements Cheque {

	private static String [] digit = {"", "one ", "two ", "three ", "four ", "five ", "six ", "seven ", "eight ", "nine "};
	private static String [] tens = {"", "ten ", "twenty ", "thirty ", "forty ", "fifty ", "sixty ", "seventy ", "eighty ", "ninety "};
	private static String [] teens = {"ten ", "eleven ", "twelve ", "thirteen ", "fourteen ", "fifteen ", "sixteen ", "seventeen ", "eighteen ", "nineteen "};
	private static String [] units = {"dollars ", "thousand ", "million "}; 
			
	public String inWords(double amount) throws ChequeException{
		// TODO implement your solution here
		String inWords; 

		//Check upper bound
		if(amount > 10000000) {
			throw new ChequeException("The cheque cannot exceed $10,000,000.", null);
		}
		
		//Check lower bound
		if(amount < 0){
			throw new ChequeException("The cheque cannot be a negative value.", null);
		}
		
		//Check decimal (no more than 2 positions)
		if(!decimalCheck2Positions(amount)){
			throw new ChequeException("The cheque cannot be have more than two decimal points.", null);
		}
		
		//Convert decimal part
		int decimal = getDecimalPart(amount);
		inWords = "and " + getValue(decimal, true) + "cents.";
		
		//Remove decimal part
		amount = amount / 1;
		
		//Convert integer part
		if(amount == 0){
			inWords = "No dollars " + inWords;
		} else if (amount == 1){
			inWords = "One dollar " + inWords;

		} else {
			int count = 0;
			while (amount > 0) {
				inWords = getValue((int)(amount % 1000), false) + units[count++] + inWords;
				amount = (int) amount / 1000;
			}
		}

		//Capitalize first letter of string and return
		return inWords.substring(0, 1).toUpperCase() + inWords.substring(1);
	}
	
	private static boolean decimalCheck2Positions(double amount) {
		String number = Double.toString(Math.abs(amount));
		int integerPlaces = number.indexOf('.');
		int decimalPlaces = number.length() - integerPlaces - 1;
		return decimalPlaces < 3;
	}
	
	private static int getDecimalPart(double amount) {
		String number = Double.toString(Math.abs(amount));
		int integerPlaces = number.indexOf('.');
		String decimal = number.substring(integerPlaces + 1, number.length());
		return Integer.parseInt(decimal);
	}
	
	private static String getValue(int number, boolean returnZero){
		String value = "";
		
		if (returnZero && number == 0){ //need to return zero for cents 
			return "no ";
		}
		if (number > 99){
			value = digit[number/100] + "hundred ";
			number = number % 100;
			if (number != 0){
				value = value + "and ";
			}
		}
		if (number > 19 || number < 10){
			value = value + tens[number / 10] + digit[number % 10];
		} else {
			value = value + teens[number % 10];
		}
		return value;
	}
}
